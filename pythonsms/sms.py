# -*- coding: utf-8 -*-
import requests


class Sms:
	def __init__(self, api_key, sms_service="smshub.org"):
		self.key = api_key
		if "http://" in sms_service or "https://" in sms_service:
			self.url = f'{sms_service}/stubs/handler_api.php'
		else:
			self.url = f'https://{sms_service}/stubs/handler_api.php'

	def request(self, action):
		try:
			params = {**{'api_key': self.key}, **action.data}
			response = requests.get(self.url, params)
			return response.text
		except (ConnectionError, TimeoutError):
			return 'NO_CONNECTION'

